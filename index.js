db.fruits.insertMany ([
	{
		name: "Apple",
		color: "Red",
		stock: 20,
		price: 40,
		supplier_id : 1,
		onSale: true,
		origin: [ "Philippines", "US"]
	},

	{
		name: "Banana",
		color: "Yellow",
		stock: 15,
		price: 20,
		supplier_id : 2,
		onSale: true,
		origin: [ "Philippines", "Ecuador"]
	},

	{
		name: "Kiwi",
		color: "Green",
		stock: 25,
		price: 50,
		supplier_id : 1,
		onSale: true,
		origin: [ "US", "China"]
	},

	{
		name: "Mango",
		color: "Yellow",
		stock: 10,
		price: 120,
		supplier_id : 2,
		onSale: false,
		origin: [ "Philippines", "India"]
	},
]);

// SECTION - MongoDB Aggregation
/*
	- Used to generate manipulated data and perform operations to create filtered results that help in analyzing data.
*/

// Using the aggregate method
/*
	Syntax: 
	db.collectionName.aggregate([
	{ $match: { fieldA, valueA} },
	{ $group: { _id: "$fieldB"}, { result: {operation}}}
]);
*/
db.fruits.aggregate([

	// $match is used to pass the documents that meet the specified condition/s to the next aggregation stage
	{ $match: {onSale: true} },

	// $group is used to group elements together and field-value pairs using the data from the grouped elements

	// $sum will total the values of all "stock" field in the returned document that are found using the $match criteria
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock"} } }
]);

// Field projection with aggregation
db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $group : { _id: "$supplier_id", total: { $sum: "$stock"} } },
	{ $project: { _id: 0}}
]);

// Sorting aggregated results
/*
	$sort - can be used to change the order of the aggregated results
	Syntax:
	{ $sort { field: 1/-1}}
	1 - ascending
	-1 - descending
*/
db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $group : { _id: "$supplier_id", total: { $sum: "$stock"} } },
	{ $sort: { total: -1} }
]);

// Aggregating results based on array fields
/*
	- $unwind - deconstructs an array field from a collection with an array value to output a result for each element

	Syntax:
	{ $unwind: field}
*/

db.fruits.aggregate ([
	{ $unwind: "$origin"}
]);

db.fruits.aggregate([
	{ $unwind: "$origin"},
	{ $group: { _id: "$origin", kinds: { $sum: 1} } }
]);


// Other Aggregate stages
// count
db.fruits.aggregate([
	{ $match: {color: "Yellow"}},
	{ $count: "Yellow Fruits"}
]);

// $avg
db.fruits.aggregate ([
	{ $match: {color: "Yellow"}},
	{ $group: {_id: "$color", yellow_fruits_stock: {$avg: "$stock"}}}
]);

// $min & $max

db.fruits.aggregate([
	{ $match: {color: "Yellow"}},
	{ $group: {_id: "$color", yellow_fruits_max_stock: {$max: "$stock"}, yellow_fruits_min_stock: {$min:"$stock"}}}
]);
